const fs = require('fs-extra');
const concat = require('concat');

(async function build() {

  const files =[
    './dist/angular-app/polyfills-es5.js',
    './dist/angular-app/polyfills-es2015.js',
    './dist/angular-app/main-es2015.js',
    './dist/angular-app/main-es5.js',
    './dist/angular-app/styles-es5.js',
    './dist/angular-app/styles-es2015.js',
  ];

  await concat(files, './dist/angular-app/main.js');
  console.info('Angular Elements created successfully!');

})();