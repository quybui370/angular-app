import {Injectable} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(protected keycloakAngular: KeycloakService) {
    }

    public login(username: string, password: string) {
    }

    public logout() {
        this.keycloakAngular.logout();
    }
}
