import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpHeaders} from '@angular/common/http';

@Injectable()
export class BaseService {

    protected basePath = environment.baseURL;
    protected defaultHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
    });

    constructor() {
    }
}
