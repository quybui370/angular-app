import {BrowserModule} from '@angular/platform-browser';
import {createCustomElement} from '@angular/elements';
import {NgModule, APP_INITIALIZER, Injector} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {ModalModule} from 'ngx-bootstrap/modal';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ApiModule} from './api.module';
import {initializer} from './utils/app-init';
import {RegisterElementModule} from './register-element.module';
import {ProductComponent} from './views/product/product.component';
import {HeaderComponent} from './views/header/header.component';
import {HomeComponent} from './views/home/home.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent,
        ProductComponent,
    ],
    imports: [
        ModalModule.forRoot(),
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        KeycloakAngularModule,
        ApiModule,
    ],
    providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
        {
            provide: APP_INITIALIZER,
            useFactory: initializer,
            deps: [KeycloakService],
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

// export class AppModule {
//     constructor(private injector: Injector) {
//     }
//
//     ngDoBootstrap(): void {
//         const {injector} = this;
//         // create custom elements from angular components
//         const headerElement = createCustomElement(HeaderComponent, {injector});
//         const productElement = createCustomElement(ProductComponent, {injector});
//         const homeElement = createCustomElement(HomeComponent, {injector});
//         // define in browser registry
//         customElements.define('ng-header', headerElement);
//         customElements.define('ng-product', productElement);
//         customElements.define('ng-home', homeElement);
//     }
// }
