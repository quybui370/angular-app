import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;
    hasError = false;

    constructor(private formBuilder: FormBuilder,
                private authService: AuthService,
                private router: Router) {
        this.loginForm = this.formBuilder.group({
            username: [''],
            password: ['']
        });
    }

    ngOnInit() {
    }

    onSubmit() {
        const result = this.authService.login(this.loginForm.get('username').value, this.loginForm.get('password').value);
        // result.subscribe((res) => {
        //     this.hasError = false;
        //     localStorage.setItem('token', res['access_token']);
        //     this.router.navigate(['/']);
        // }, (error) => {
        //     this.hasError = true;
        // });
        // this.submitted = true;
    }
}
