import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalService, BsModalRef} from 'ngx-bootstrap/modal';
import {FormBuilder, FormGroup} from '@angular/forms';
import {KeycloakService} from 'keycloak-angular';
import {Product} from '../../model/models';
import {ProductsService} from '../../services/products.service';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
    modalRef: BsModalRef;
    productForm: FormGroup;
    private productList: Product[] = [];
    private activeProductID: string;

    constructor(private productService: ProductsService,
                private modalService: BsModalService,
                private formBuilder: FormBuilder,
                protected keycloakAngular: KeycloakService) {
        this.productForm = this.formBuilder.group({
            name: '',
            price: '',
            category: ''
        });
        const result = productService.productListGet();
        result.subscribe((res) => {
            this.productList = res.data;
        }, (error) => {
            console.log(error);
        });
    }

    ngOnInit() {
    }

    private viewProductDetails(id: string) {
        document.getElementsByTagName('react-product-details')[0].setAttribute("productid", id);
    }

    private deleteProduct(activeProduct: Product) {
        const result = this.productService.productProductIdDelete(activeProduct._id);
        result.subscribe(res => {
            const index = this.productList.indexOf(activeProduct);
            this.productList.splice(index, 1);
        }, error => {
            console.log(error);
        });
    }

    private openModal(template: TemplateRef<any>, activeProduct?: Product) {
        this.modalRef = this.modalService.show(template);
        if (activeProduct) {
            this.activeProductID = activeProduct._id;
            this.productForm.setValue({
                name: activeProduct.name,
                price: activeProduct.price,
                category: activeProduct.category
            });
        }
    }

    private submitForm(action: string) {
        let result;
        switch (action) {
            case 'create':
                result = this.productService.productNewPost(this.productForm.value);
                result.subscribe(res => {
                    this.productList.push(this.productForm.value);
                    this.productForm.setValue({
                        name: '',
                        price: '',
                        category: ''
                    });
                    this.modalRef.hide();
                }, error => {
                    console.log(error);
                });
                break;
            case 'update':
                result = this.productService.productProductIdPut(this.activeProductID, this.productForm.value);
                result.subscribe(res => {
                    const product = this.productList.find(p => p._id === this.activeProductID);
                    product.name = this.productForm.value.name;
                    product.price = this.productForm.value.price;
                    product.category = this.productForm.value.category;
                    this.productForm.setValue({
                        name: '',
                        price: '',
                        category: ''
                    });
                    this.modalRef.hide();
                }, error => {
                    console.log(error);
                });
                break;
            default:
                break;
        }
    }
}
