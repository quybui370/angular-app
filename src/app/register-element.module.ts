import {NgModule, Injector} from '@angular/core';
import {CommonModule} from '@angular/common';
import {createCustomElement} from '@angular/elements';
import {ProductComponent} from './views/product/product.component';
import {HeaderComponent} from './views/header/header.component';
import {HomeComponent} from './views/home/home.component';

@NgModule({
    declarations: [
        ProductComponent,
        HeaderComponent,
        HomeComponent,
    ],
    imports: [
        CommonModule
    ],
    bootstrap: [],
    entryComponents: [
        ProductComponent,
        HeaderComponent,
        HomeComponent,
    ]
})
export class RegisterElementModule {
    constructor(private injector: Injector) {
    }

    ngDoBootstrap(): void {
        const {injector} = this;
        // create custom elements from angular components
        const headerElement = createCustomElement(HeaderComponent, {injector});
        // define in browser registry
        customElements.define('ng-header', headerElement);
    }
}
