import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './views/login/login.component';
import {ProductComponent} from './views/product/product.component';
import {HomeComponent} from './views/home/home.component';
import {CanAuthenticationGuard} from './services/auth-guard.service';

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'products', component: ProductComponent, canActivate: [CanAuthenticationGuard]},
    {path: '', component: HomeComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})

export class AppRoutingModule {
}
