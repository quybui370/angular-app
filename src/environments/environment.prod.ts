import { KeycloakConfig } from 'keycloak-angular';

const keycloakConfig: KeycloakConfig = {
    url: 'http://localhost:8080/auth',
    realm: 'nodejs-keycloak',
    clientId: 'nodejs-connect',
    // credentials: {
    //   secret: '4caa9942-443e-4429-b292-f4da04c98bfd'
    // }
};
export const environment = {
    production: true,
    baseURL: 'http://localhost',
    keycloak: keycloakConfig,
};
